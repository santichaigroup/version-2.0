<div class="row" style="background-color:#F3F8FE; border-bottom:1px solid #ccc; padding:5px;">
    <div class="container">
        <div class="row"><a href="languages.php"><?php echo
                $translated_panel_strings['Languages'];
                ?></a> | <a href="translations.php"><?php echo $translated_panel_strings['Translations']; ?></a>
            | <a href="panel_languages.php"><?php echo
                $translated_panel_strings['PanelLanguages'];
                ?></a> | <a
                href="panel_translations.php"><?php echo $translated_panel_strings['PanelTranslations']; ?></a>
            | <a
                href="edit_access_credentials.php"><?php echo $translated_panel_strings['EditAccessCredentials']; ?></a>
            | <a href="logout.php"><?php
                echo $translated_panel_strings['Logout']; ?></a></div>
    </div>
</div>