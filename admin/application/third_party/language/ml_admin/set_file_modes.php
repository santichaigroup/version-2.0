<?php
/**
 * This file is used for easily set write permissions to the following files and directories:
 * - "access_credentials.php"
 * you can delete this file once you have correct settings.
 */

require_once('config/panel.php');

if (!is_writable('access_credentials.php')) {
    if (file_exists('access_credentials.php')) {
        die("'admin/access_credentials.php' doesn't exits!");
    } else {
        if (!chmod('access_credentials.php', 0755)) {
            die("can't set write permission for 'admin/access_credentials.php'!");
        }
    }
}

echo "ALL FILES HAVE CORRECT SETTINGS NOW!";