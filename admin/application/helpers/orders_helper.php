<?php
if(!function_exists('create_order')) {
	//=========================================================================================================================
	// $order array key
	//	require
	//	- customer_id, customer_name, order_channel, order_subtotal, order_grand_total, order_datetime, ip_address, shipping_address_id, payment_method
	//
	//	optional
	//	- staff_id [staff id], staff_name, order_coupon_code, order_discount
	//
	//	example
	//	- $order = array(
	//		  'customer_id' => 1, (member id)
	//		  'customer_name' => 'Maliwan Duenphen', 
	//		  'order_channel' => '000', (000 = web, 001, call center, 002 = shop)
	//		  'order_subtotal' => 135.00, (price before discount or add tax (tax if require)) 
	//		  'order_grand_total' => 135.00, (total price to customer pay)
	//		  'order_datetime' => '1990-01-01 00:00:00', (date and time)
	//		  'ip_address' => '127.0.0.1', 
	//		  'shipping_address_id' => 1, 
	//		  'shipping_method' => 'thailandpost', 
	//		  'shipping_cost' => 30.00
	//		  'payment_method' => 'paysbuy', 
	//
	//		  'staff_id' => 35, (staff id)
	//		  'staff_name' => 'Malila Ladphrao', 
	//		  'order_coupon_code' => 'SPECIALCOUPON2015', 
	//		  'order_discount' => 0.00,  (discount price)
	//		  'order_tax_cost' => 9.45 (tax)
	//	  );
	//
	// $order_option array key
	//	require
	//	- option_name, option_value
	//
	//	example
	//	- $order_option = array( 
	//	      'delivery_first' => '2015-09-02', 
	//	      'delivery_last', => '2015-09-05' 
	//	  );
	//
	// $item array key
	//	require
	//	- item_id, item_sku, order_quantity, order_price
	//
	//  optional
	//  - remark
	//
	//	example
	//	- $item = array(
	//	      array('item_id' => 1, 'item_sku' => 'RICE001', 'order_quantity' => 10, 'order_price' => 30.00, 'remark' => 'ต่างๆ นาๆ'), 
	//	      array('item_id' => 2, 'item_sku' => 'RICE020', 'order_quantity' => 2, 'order_price' => 105.00, 'remark' => 'นานาจิตตัง')
	//	  );
	//
	// $item_option array key
	//	require
	//	- item_id, option_name, option_value
	//
	//	example
	//	- $item_option_list = array(
	//		array('item_id' => 1, 'options' => array('repast' => 'มื้อเช้า', 'delivery' => '2015-09-02')), 
	//		array('item_id' => 2, 'options' => array('repast' => 'มื้อกลางวัน', 'delivery' => '2015-09-02')), 
	//		array('item_id' => 3, 'options' => array('repast' => 'มื้อเย็น', 'delivery' => '2015-09-02'))
	//	  );
	//=========================================================================================================================
	function create_order($order = array(), $order_option = array(), $item = array(), $item_option = array()) {
		$_return = array();
		$_order_item_option = array();

		$CI =& get_instance();
		$CI->load->model('orders_model');

		// create order
		$_order_id = $CI->orders_model->create_order($order);

		// check order create compelte
		if($_order_id['status'] === true) {
			// create order referrence code
			$_order_length = strlen($_order_id['order_id']);
			for($i = 1; $i <= (20 - $_order_length); $i++) {
				$_order_ref_id = $_order_ref_id . '0';
			}

			$_order_ref_id = '#' . $_order_ref_id . $_order_id['order_id'];

			$ref_code_param = array(
				'order_ref_id' => $_order_ref_id, 
				'order_id' => $_order_id['order_id']
			);

			$_order_ref_id = $CI->orders_model->create_order_reference_code($ref_code_param);

			// create order  option
			$_order_option_param = array();
			foreach ($order_option as $key => $value) {
				$_order_option_param[] = array(
					'order_id' => $_order_id['order_id'], 
					'option_name' => $key, 
					'option_value' => $value
				);
			}

			$CI->orders_model->create_order_option($_order_option_param);

			// create item in order
			$insert_flag = true;
			$_order_item_option_param = array();

			foreach ($item_list as $key => $value) {
				$_order_item_param = array(
					'order_id' => $_order_id['order_id'], 
					'item_id' => $value['item_id'], 
					'item_sku' => $value['item_sku'], 
					'order_quantity' => $value['order_quantity'], 
					'order_price' => $value['order_price'], 
					'remark' => $value['remark']
				);

				$_order_item = $CI->orders_model->create_order_items($_order_item_param);

				// check item can insert
				if($_order_item['status'] === false) {
					$insert_flag = false;
					break;
				}
				else
				{
					foreach ($item_option as $k => $v) {
						if($v['item_id'] == $value['item_id']) {
							foreach ($v['options'] as $k1 => $v1) {
								$_order_item_option_param[] = array(
									'order_item_id' => $_order_item['order_item_id'], 
									'option_name' => $k1, 
									'option_value' => $v1
								);
							}
						}
					}
				}
			}

			// check insert item in order
			if($insert_flag === false) {
				// insert incomplete
				$CI->orders_model->delete_order($_order_id['order_id']);
				$CI->orders_model->delete_order_item($_order_id['order_id']);

				$_return = array(
					'success' => false, 
					'message' => 'Order not complete', 
					'order_id' => 0, 
					'order_reference_code' => ''
				);
			}
			else {
				// insert complete
				// create order item option
				$CI->orders_model->create_order_item_option($_order_item_option_param);

				// create order log
				$_order_log_param = array(
					'order_id' => $_order_id['order_id'], 
					'action_text' => 'Order Complete and wating for payment', 
					'log_datetime' => $order['order_datetime']
				);

				$CI->orders_model->create_order_logs($_order_log_param);

				$_return = array(
					'success' => false, 
					'message' => 'Order complete', 
					'order_id' => $_order_id['order_id'], 
					'order_reference_code' => $_order_ref_id['order_ref_id']
				);
			}
		}
		else {
			$_return = array(
				'success' => false, 
				'message' => $_order_id['message'], 
				'order_id' => 0, 
				'order_reference_code' => ''
			);
		}

		return $_return;
	}
}

if(! function_exists('create_order_log')) {
	function create_order_log($order_id, $log_message) {
		$_return = array();

		$CI =& get_instance();
		$CI->load->model('orders_model');

		$_datetime = date('Y-m-d H:i:s', time());
		$_date_and_time = explode(' ', $_datetime);

		$_order_log_param = array(
			'order_id' => $order_id, 
			'action_text' => $log_message, 
			'log_datetime' => $_datetime, 
			'log_date' => $_date_and_time[0], 
			'log_time' => $_date_and_time[0]
		);

		// create order log
		$CI->orders_model->create_order_logs($_order_log_param);

		$_return = array(
			'success' => false, 
			'message' => 'Create order log complete'
		);

		return $_return;
	}
}

if(! function_exists('update_payment_ref_code')) {
	function update_payment_ref_code($order_id, $payment_ref_code) {
		$_return = array();

		$CI =& get_instance();
		$CI->load->model('orders_model');

		$_result = $CI->orders_model->update_payment_ref_code($order_id, $payment_ref_code);

		if($_result === false) {
			$_return = array(
				'success' => false, 
				'message' => 'Can\'t update payment reference code.'
			);
		}
		else {
			$_return = array(
				'success' => true, 
				'message' => 'Update payment reference code complete.'
			);
		}

		return $_return;
	}
}

if(! function_exists('get_order_option')) {
	function get_order_option($order_id) {
		$_return = array();

		$CI =& get_instance();
		$CI->load->model('orders_model');

		$_order_option = $CI->orders_model->get_order_option($order_id);
		foreach ($_order_option as $key => $value) {
			$_return[$value['option_name']] = $value['option_value'];
		}

		return $_return;
	}
}

if(! function_exists('get_order_item_option')) {
	function get_order_item_option($order_item_id) {
		$_return = array();

		$CI =& get_instance();
		$CI->load->model('orders_model');

		$_order_item_option = $CI->orders_model->get_order_item_option($order_item_id);
		foreach ($_order_item_option as $key => $value) {
			$_return[$value['option_name']] = $value['option_value'];
		}

		return $_return;
	}
}

if(! function_exists('datetime_thai')) {
	function datetime_thai($strDate, $format = 'full') {
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));

		if($format == 'short')
		{
			$strMonthCut = array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$strYear = substr($strYear, 2, 2);
		}
		else if($format == 'full')
		{
			$strMonthCut = array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		}
		else
		{
			$strMonthCut = '';
		}

		$strMonthThai = $strMonthCut[$strMonth];

		return "$strDay $strMonthThai $strYear เวลา $strHour:$strMinute น.";
	}
}