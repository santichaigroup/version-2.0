<?php
if(! function_exists('create_slug')) {
	function create_slug($text)
	{
		$array = array("'", '"', '\\', '/', '.', ',', '!', '+', '*', '%', '$', '#', '@', '(', ')', '[', ']', '?', '>', '<', '|', '’', '“', '”', '‘', '’', ';', ':', '–');
		$text = str_replace($array, '', $text); // Replaces all spaces with hyphens.
		$text = preg_replace('/[^A-Za-z0-9ก-๙เ-า\\-]/', '-', $text); // Removes special chars.
		$text = preg_replace('/-+/', "-", $text);
		$text = trim($text, '-');
		$text = strtolower($text);

		if (empty($text))
		{
			return 'n-a';
		}

		return $text;
	}
}