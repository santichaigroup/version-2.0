<?php
if(! function_exists('get_data')){
	/*
	get data function
	$cache_name : define name of cache to store in folder (name has encrypted in cache folder)
	$model_name : model to call data
	$function : function in model
	$param : function parameter ex. array($parameter1, $parameter2, $parameter3, ..., $parameterN)
	$cache_time : time to cache (seconds)
	*/
	function get_data($cache_name = "", $model = "", $function = "", $param = array(), $cache_time = 3600){
		$CI =& get_instance();
		$CI->load->driver('cache');

		$extract_model_path = explode('/', $model);
		$model_name = $extract_model_path[count($extract_model_path) - 1];
		
		if ( ! class_exists($model_name) ) {
			$CI->load->model($model);
		}

		$_cache_data = $CI->cache->file->get(md5($cache_name));
		
		if($_cache_data === FALSE)
		{
			$_data_list = call_user_func_array(array($CI->{$model_name}, $function), $param);
			if(is_object($_data_list) AND strpos(get_class($_data_list), 'CI_DB') == 0){
				$_data_list = $_data_list->result_array();
			}

			$CI->cache->file->save(md5($cache_name), $_data_list, $cache_time);
		}
		else
		{
			$_data_list = $_cache_data;
		}

		return $_data_list;
	}
}

if(! function_exists('delete_data')){
	/*
	delete data function
	$cache_name : name of cache to delete
	*/
	function delete_data($cache_name = ""){
		$CI =& get_instance();
		$CI->load->driver('cache');

		return $CI->cache->file->delete(md5($cache_name));
	}
}

if(! function_exists('clear_data')){
	/*
	clear data function
	clear all cache data
	*/
	function clear_data($cache_name = ""){
		$CI =& get_instance();
		$CI->load->driver('cache');

		return $CI->cache->file->clean(md5($cache_name));
	}
}