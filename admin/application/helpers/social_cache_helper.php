<?php
if(! function_exists('social_cache')) {
	function social_cache($url) {
		$url = 'https://graph.facebook.com/';
		$vars = 'id=' . urlencode($url) . '&scrape=true';

		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_POST, 1);
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $vars);
		curl_setopt( $ch, CURLOPT_HEADER, 0);
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

		$response = curl_exec( $ch );
		curl_close($ch);

		return $response;
	}
}