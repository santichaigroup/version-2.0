<?php
class Member_admin_model extends CI_Model {

	public function dataTable()
	{
		$this->db->select('member.*, member_class.class_name AS class_name');
		$this->db->from('member');

		$this->db->join("member_class","member.member_class=member_class.id", "left");

		$this->db->where("member.status <>","deleted");	
		$this->db->order_by("member.sequence","ASC");
		$this->db->limit(1000);
		return $this->db->get();
	}

	public function dataTable_class()
	{
		$this->db->select('*');
		$this->db->from('member_class');

		$this->db->where("member_class.class_status <>","deleted");
		$this->db->limit(1000);
		return $this->db->get();
	}

	public function checkDefaultLang($member_id)
	{
		$this->db->where("member_id",$member_id);
		$query = $this->db->get("member")->row_array();
		
		$this->db->where("content_id",$query['default_content_id']);
		$lang_default = $this->db->get("member_content")->row_array();
		
		return $lang_default['lang_id'];
	}
	public function checkExistst($member_id)
	{
		$this->db->where("member.member_id",$member_id);
		$this->db->where("member.status <>","deleted");
		return $this->db->count_all_results("member");
	}

	public function checkExiststClass($member_id)
	{
		$this->db->where("member_class.id",$member_id);
		$this->db->where("member_class.class_status <>","deleted");
		return $this->db->count_all_results("member_class");
	}

	public function checkdataTable()
	{
		return $this->db->get("member");
	}
	public function getDetail($member_id,$lang_id=NULL)
	{
		$this->db->select('member.*, member_class.class_name AS class_name');
		$this->db->from('member', 'member_content');
		
		// if($lang_id){
		// 	$this->db->join("member_content","member_content.member_id = member.member_id AND member_content.lang_id = '".$lang_id."'");
		// }else{
		// 	$this->db->join("member_content","member_content.member_id = member.member_id AND member_content.content_id = member.default_content_id");
		// }
		$this->db->join("member_class","member.member_class=member_class.id", "left");
		
		$this->db->where("member.member_id",$member_id);
		$this->db->where("member.status <>",'deleted');
		$this->db->order_by("member.member_id","desc");
		$this->db->limit(1000);
		return  $this->db->get()->row_array();
						
	}

	public function getDetailClass($member_id,$lang_id=NULL)
	{
		$this->db->select('*');
		$this->db->from('member_class');
		$this->db->where('id', $member_id);
		$this->db->where('class_status <>', 'deleted');

		return  $this->db->get()->row_array();
	}

	public function get_address_member($member_id=NULL, $address_id=NULL)
	{
		$this->db->select('member_address.*, system_province.province_name_th AS province_name, system_district.district_name_th AS district_name, system_sub_district.sub_district_name_th AS sub_district_name');
		$this->db->from('member_address');

		if($member_id) {
			$this->db->where('member_id', $member_id);
		}

		if($address_id) {
			$this->db->where('id', $address_id);
		}

		$this->db->join('system_province', 'member_address.province_id=system_province.province_id', 'left');
		$this->db->join('system_district', 'member_address.district_id=system_district.district_id', 'left');
		$this->db->join('system_sub_district', 'member_address.sub_district_id=system_sub_district.sub_district_id', 'left');

		$this->db->where('address_status <>', 'deleted');
		return $this->db->get();
	}

	public function addData()
	{
		$this->db->where("status <>","deleted");
		$numrow = $this->db->get('member')->num_rows();
		($numrow==0)?$sequence = 1 : $sequence = $numrow+1;
		
		$this->db->set("sequence",$sequence);
		$this->db->set("post_date","NOW()",false);
		$this->db->set("post_ip",$this->input->ip_address());
		$this->db->set("post_by",$this->admin_library->userdata('user_id'));
		$this->db->insert("member");
		$member_id = $this->db->insert_id();
		if(!$member_id){
			show_error("Cannot create  member id");	
		}
		return $member_id;
	}
	public function setDate($member_id,$main_date)
	{
		$main_date = date("Y-m-d",strtotime($main_date));
		$this->db->set("main_date",$main_date);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("member_id",$member_id);
		return $this->db->update("member");	
	}
	public function setStatus($member_id,$status)
	{
		if($status=="deleted")
		{
			$this->db->where("status <>","deleted");
			$this->db->where("sequence <>","0");
			$numrow = $this->db->get('member')->num_rows();
			if($numrow>0)
			{
				
				$this->db->where("member_id",$member_id);
				$this->db->where("status <>","deleted");
				$row_sequence = $this->db->get("member")->row_array();
				$old_sequence = $row_sequence['sequence'];
				$new_sequence = $numrow;
				for ($ias = ($old_sequence+1); $ias <=$new_sequence; $ias++)
				{
					$iup = $ias-1;
					$this->db->set("sequence",$iup);
					$this->db->where("sequence",$ias);
					$this->db->where("sequence <>",0);
					$this->db->where("status <>","deleted");
					$this->db->update("member");
				
				}
			}
			
		}
		
		if($status=="deleted")
		{
			$this->db->set("sequence",0);
		}
		
		
		$this->db->set("status",$status);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("member_id",$member_id);
		return $this->db->update("member");	
	}
	public function setDefaultContent($member_id,$content_id)
	{
		$this->db->set("default_content_id",$content_id);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("member_id",$member_id);
		return $this->db->update("member");
	}
	public function addLanguage($member_id,$lang_id)
	{//
		$this->db->where("member_id",$member_id);
		$this->db->where("lang_id",$lang_id);
		$has = $this->db->get("member_content");
		
		if($has->num_rows() == 0){
			$this->db->set("member_id",$member_id);
			$this->db->set("lang_id",$lang_id);
			$this->db->insert("member_content");
			return $this->db->insert_id();
		}else{
			$r= $has->row_array();
			return 	@$r['content_id'];
		}
		
	}
	function updateContent($member_id,$lang_id,$subject,$description,$detail,$content_keyword,$content_thumbnail=NULL)
	{
		
		if($content_thumbnail){
			$this->db->set("content_thumbnail",$content_thumbnail);
		}

		$this->db->set("content_keyword",$content_keyword);
		$this->db->set("content_subject",$subject);
		$this->db->set("content_description",$description);
		$this->db->set("content_detail",$detail);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("member_id",$member_id);
		$this->db->where("lang_id",$lang_id);
		return $this->db->update("member_content");
	}
	function updateContentStatus($member_id,$status)
	{
		$this->db->set("status",$status);
		$this->db->where("member_id",$member_id);
		return $this->db->update("member");
	}

	function addClass($class_name, $class_detail, $class_status)
	{
		$this->db->set("class_name", $class_name);
		$this->db->set("class_detail", $class_detail);
		$this->db->set("class_status", $class_status);

		$this->db->insert("member_class");

		return $this->db->insert_id();
	}

	function addClassAdjustmentValue($member_class_id, $adjust_value, $how_to_adjust = 'member_class_adjust_value')
	{
		$this->db->set("member_class_id", $member_class_id);
		$this->db->set("how_to_adjust", $how_to_adjust);
		$this->db->set("adjust_value", $adjust_value);

		return $this->db->insert("member_class_definition");
	}

	function updateClass($member_id, $class_name, $class_detail, $class_status)
	{
		$this->db->set("class_name", $class_name);
		$this->db->set("class_detail", $class_detail);
		$this->db->set("class_status", $class_status);
		$this->db->where("id",$member_id);
		return $this->db->update("member_class");
	}

	function updateClassAdjustmentValue($member_class_id, $adjust_value, $how_to_adjust = 'member_class_adjust_value')
	{
		$this->db->set("how_to_adjust", $how_to_adjust);
		$this->db->set("adjust_value", $adjust_value);

		$this->db->where("member_class_id",$member_class_id);
		return $this->db->update("member_class_definition");
	}

	function getClassAdjustmentValue($member_class_id) {
		$this->db->where('member_class_id', $member_class_id);

		return $this->db->get('member_class_definition')->row_array();
	}

	public function delData($member_id)
	{
		$this->db->where("status <>","deleted");
		$this->db->where("sequence <>","0");
		$numrow = $this->db->get('member')->num_rows();
		if($numrow>0)
		{
			
			$this->db->where("member_id",$member_id);
			$this->db->where("status <>","deleted");
			$row_sequence = $this->db->get("member")->row_array();
			$old_sequence = $row_sequence['sequence'];
			$new_sequence = $numrow;
			for ($ias = ($old_sequence+1); $ias <=$new_sequence; $ias++)
			{
				$iup = $ias-1;
				$this->db->set("sequence",$iup);
				$this->db->where("sequence",$ias);
				$this->db->where("sequence <>",0);
				$this->db->where("status <>","deleted");
				$this->db->update("member");
			
			}
		}
		
		$this->db->set("status","deleted");
		$this->db->where("member_id",$member_id);
		return $this->db->update("member");		
	}

	public function delDataClass($member_id)
	{
		$this->db->set("class_status","deleted");
		$this->db->where("id",$member_id);

		return $this->db->update("member_class");
	}

	function deleteImage($img_id,$img_name) 
	{
		
		$this->db->where("img_id",$img_id);
		return $this->db->delete("member_img");
	}
	function setImgSequence($id,$i)
	{
		$this->db->set("img_sequence",$i);	
		$this->db->set("img_update","NOW()",false);
		$this->db->where("img_id",$id);
		
		return $this->db->update("member_img");
		
	}
	function set_sequence($member_id,$old_sequence,$new_sequence)
	{
		$ias = 0; 
		$iup = 0;
		$sumx = $old_sequence-$new_sequence; 
		
		if($sumx < 0)
		{
			 	for ($ias = ($old_sequence+1); $ias <=$new_sequence; $ias++)
				{
					$iup = $ias-1;
					$this->db->set("sequence",$iup);
					$this->db->where("sequence",$ias);
					$this->db->where("sequence <>",0);
					$this->db->where("status <>","deleted");
					$this->db->update("member");
				
				}
		
		}
		if($sumx > 0)
		{
			 	for ($ias = ($old_sequence-1); $ias >= $new_sequence; $ias--)
				{
					$iup = $ias +1; 
					$this->db->set("sequence",$iup);
					$this->db->where("sequence",$ias);
					$this->db->where("sequence <>",0);
					$this->db->where("status <>","deleted");
					$this->db->update("member");
				
				}
		
		}
		
		$this->db->where("status <>","deleted");
		$this->db->where("sequence <>",0);
		$this->db->select('sequence');
		$sequence_max = $this->db->get("member")->num_rows();

			if($new_sequence > $sequence_max){
				$new_sequence = $sequence_max;
			}
		
			if($new_sequence < 1){ 
				$new_sequence = 1;
			}
			
		
		$this->db->set("sequence",$new_sequence);
		$this->db->where("member_id",$member_id);
		return $this->db->update("member");
	}
	
	function dateTimestamp($datetime=NULL)
	{
		$day = @explode("-",$datetime);
		$y = $day[0];
		$m = $day[1];
		$d = $day[2];
		$timestamp = mktime(0, 0, 0, $m, $d, $y);
		return $timestamp ;//date('Y-m-d',$timestamp);	
	}

	public function get_order_member_id($member_id) {
		if(empty($member_id) == true) {
			return array();
		}
		else {
			$this->db->join('order_items', 'orders.id=order_items.order_id');
			$this->db->where('customer_id', $member_id);
			$_result = $this->db->get('orders');
			
			return $_result->result_array();
		}
	}

}