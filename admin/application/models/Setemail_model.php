<?php
class Setemail_model extends CI_Model{
	public function dataTable()
	{//

		$this->db->order_by("content_id","desc");
		$this->db->limit(1);
		return $this->db->get('setemail_content');
	}
	
	public function checkdataTable()
	{
		return $this->db->get("setemail_content");
	}
	public function getDetail($content_id)
	{
		$this->db->where("content_id",$content_id);
		$this->db->limit(1);
		return $this->db->get('setemail_content')->row_array();
	}
	public function addData()
	{
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->set("update_by",$this->admin_library->userdata('user_id'));
		$this->db->insert("setemail_content");
		$content_id = $this->db->insert_id();
		if(!$content_id){
			show_error("Cannot create  setemail id");	
		}
		return $content_id;
	}
	

	
	function updateContent($content_id,$content_detail,$content_email,$content_cc,$mail_paysbuy,$psbID,$pin,$secureCode)
	{
		$this->db->set("content_detail",$content_detail);
		$this->db->set("content_cc",$content_cc);
		$this->db->set("content_email",$content_email);
		$this->db->set("content_mail_paysbuy",$mail_paysbuy);
		$this->db->set("content_psbID",$psbID);
		$this->db->set("content_pin",$pin);
		$this->db->set("content_secureCode",$secureCode);
		$this->db->set("update_date","NOW()",false);
		$this->db->set("update_ip",$this->input->ip_address());
		$this->db->where("content_id",$content_id);
		return $this->db->update("setemail_content");
	}
	
}