<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $email_title_order_confirmation; ?></title>
  <meta name="description" content="">

  <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
  <link rel="icon" type="image/png" href="<?php echo base_url('public'); ?>/favicon/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo base_url('public'); ?>/favicon/favicon-194x194.png" sizes="194x194">
  <link rel="icon" type="image/png" href="<?php echo base_url('public'); ?>/favicon/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo base_url('public'); ?>/favicon/android-chrome-192x192.png" sizes="192x192">
  <link rel="icon" type="image/png" href="<?php echo base_url('public'); ?>/favicon/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="<?php echo base_url('public'); ?>/favicon/manifest.json">
  <link rel="mask-icon" href="<?php echo base_url('public'); ?>/favicon/safari-pinned-tab.svg" color="#5bbad5">
  <link rel="shortcut icon" href="<?php echo base_url('public'); ?>/favicon/favicon.ico">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="msapplication-TileImage" content="<?php echo base_url('public'); ?>/favicon/mstile-144x144.png">
  <meta name="msapplication-config" content="<?php echo base_url('public'); ?>/favicon/browserconfig.xml">
  <meta name="theme-color" content="#ffffff">
  <style>
    body {
      padding: 0;
      margin: 0;
    }
  </style>
</head>

<body>

<table style="width: 640px; margin-left: auto; margin-right: auto; border-collapse: collapse; font-size: 16px; color: #686868; font-family: tahoma;">
  <thead>
    <tr>
      <th><img src="<?php echo base_url('public'); ?>/img/email_header.jpg" width="640" alt="Q Fresh"></th>
    </tr>
    <tr>
      <th>
        <div style="color: #1f72a3; font-size: 28px; text-align: center; padding-bottom: 15px; font-weight: normal;"><?php echo $text_lang['order_confirmation_header']; ?></div>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="border-bottom: 1px solid #e1e1e1; padding: 10px 0; padding-left: 15px; ">
        <table>
          <tr>
            <td style="vertical-align: top; padding: 5px; width: 90px" rowspan="2"><b><?php echo $text_lang['order_confirmation_detail']; ?></b></td>
            <td style="vertical-align: top; padding: 5px;" colspan="2"><b><?php echo $text_lang['order_confirmation_customer']; ?></b> : <?php echo $customer_name; ?></td>
          </tr>
          <tr>
            <td style="vertical-align: top; padding: 5px; width: 200px"><b><?php echo $text_lang['order_confirmation_order_date']; ?></b> : <?php echo $order_date; ?></td>
            <td style="vertical-align: top; padding: 5px;"><b><?php echo $text_lang['order_confirmation_order_number']; ?></b> : #<?php echo $order_ref_id; ?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="border-bottom: 1px solid #e1e1e1; padding: 10px 0; padding-left: 15px;">
        <table>
          <tr>
            <td style="vertical-align: top; padding: 5px; width: 90px" rowspan="2"><b><?php echo $text_lang['order_confirmation_shipping']; ?></b></td>
            <td style="vertical-align: top; padding: 5px;"><?php echo $shipping_address; ?></td>
          </tr>
          <!--<tr>
            <td style="vertical-align: top; padding: 5px;">Tel: 0-2354-3547</td>
          </tr>-->
        </table>
      </td>
    </tr>
    <tr>
      <td style="padding: 10px 0;  padding-left: 15px;">
        <table>
          <tr>
            <td style="vertical-align: top; padding: 5px; width: 90px" rowspan="2"><b><?php echo $text_lang['order_confirmation_billing']; ?></b></td>
            <td style="vertical-align: top; padding: 5px;"><?php echo $billing_address; ?></td>
          </tr>
          <!--<tr>
            <td style="vertical-align: top; padding: 5px;">Tel: 0-2354-3547</td>
          </tr>-->
        </table>
      </td>
    </tr>
    <tr>
      <td>
        <table style="width: 100%; border-collapse: collapse;">
          <thead style="font-size: 13px;">
            <tr>
              <th style="border: 1px solid #e1e1e1; border-left: none; border-right: none; background-color: #f9f9f9; padding: 10px; padding-left: 30px; width: 350px;text-align: left;"><?php echo $text_lang['order_confirmation_product_hd']; ?></th>
              <th style="border: 1px solid #e1e1e1; border-left: none; border-right: none; background-color: #f9f9f9; padding: 10px;"><?php echo $text_lang['order_confirmation_price_unit_hd']; ?></th>
              <th style="border: 1px solid #e1e1e1; border-left: none; border-right: none; background-color: #f9f9f9; padding: 10px;"><?php echo $text_lang['order_confirmation_unit_hd']; ?></th>
              <th style="border: 1px solid #e1e1e1; border-left: none; border-right: none; background-color: #f9f9f9; padding: 10px; padding-right: 30px;"><?php echo $text_lang['order_confirmation_total_hd']; ?></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach($result_order_item as $key => $value): ?>
            <tr>
              <td style="padding-left: 15px; vertical-align: top; border-bottom: 1px dotted #bfbfbf ;">
                <div style="padding: 10px;">
                  <table style="width: 100%; border-collapse: collapse;">
                    <tr>
                      <td style="width: 95px;vertical-align: top;"><img src="<?php echo $value['product_image']; ?>" width="95" alt=""></td>
                      <td style="padding-left: 15px; vertical-align: top;">
                        <div style="font-size: 14px; color: #4b4b4b; font-weight: bold;"><?php echo $value['item_name']; ?></div>
                        <!--<div style="font-size: 11px;">Lorem ipsum dolor sit amet, con sectetur adipiscing elit. </div>-->
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
              <td style="vertical-align: top; text-align: center; border-bottom: 1px dotted #bfbfbf ;">
                <div style="padding: 10px;"><?php echo $value['order_price'] / $value['order_quantity']; ?></div>
              </td>
              <td style="vertical-align: top; text-align: center; border-bottom: 1px dotted #bfbfbf ;">
                <div style="padding: 10px;"><?php echo $value['order_quantity']; ?></div>
              </td>
              <td style="padding-right: 15px; vertical-align: top; text-align: right;  border-bottom: 1px dotted #bfbfbf ;">
                <div style="padding: 10px;"><?php echo $value['order_price']; ?></div>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <table style="width: 100%; border-collapse: collapse; font-size: 12px; color: #444444;">
          <tr>
            <td style="padding: 5px; padding-left: 25px; padding-top: 15px;"><?php echo $text_lang['order_confirmation_subtotal']; ?></td>
            <td style="padding: 5px; text-align: right;  padding-right: 15px; padding-top: 15px;"><div style="font-size: 16px; font-weight: bold;"><?php echo $order_subtotal; ?></div></td>
          </tr>
          <tr>
            <td style="padding: 5px; padding-left: 25px;"><?php echo $text_lang['order_confirmation_shipping_cost']; ?></td>
            <td style="padding: 5px; text-align: right;  padding-right: 15px"><div style="font-size: 16px; font-weight: bold; color: #a3cd48;"><?php echo $shipping_cost == 0 ? 'FREE' : $shipping_cost; ?></div></td>
          </tr>
          <tr>
            <td style="padding: 5px; padding-left: 25px; padding-bottom: 15px;"><?php echo $text_lang['order_confirmation_promocode']; ?></td>
            <td style="padding: 5px; text-align: right;  padding-right: 15px; padding-bottom: 15px;"><div style="font-size: 16px; font-weight: bold; color: #f04c23">-<?php echo $order_discount; ?></div></td>
          </tr>
          <tr>
            <td style="padding: 15px; border: 1px solid #e1e1e1; border-left: none; border-right: none; padding-left: 25px; color: #ff7e00;"><div><?php echo $text_lang['order_confirmation_total']; ?></div></td>
            <td style="padding: 15px; border: 1px solid #e1e1e1; border-left: none; border-right: none; text-align: right; padding-right: 15px; color: #ff7e00;"><div style="font-size: 21px; font-weight: bold;"><?php echo $order_grand_total; ?></div></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td style="padding: 10px; padding-left: 25px; border-bottom: 1px solid #e1e1e1;">
        <p><b><?php echo $text_lang['order_confirmation_order_status']; ?> : </b><?php echo $text_lang['order_status'][$order_status]; ?></p>
        <!--
        <p><b><?php echo $text_lang['order_confirmation_payment_status']; ?> : </b>รอชำระเงิน</p>
        <p><b><?php echo $text_lang['order_confirmation_payment_method']; ?> : </b>เครดิตการ์ด <img src="<?php echo base_url('public'); ?>/img/email_paymentIcon.png" alt=""></p>
        <p><b><?php echo $text_lang['order_confirmation_shipping_status']; ?> : </b>รอชำระเงิน</p>
        -->
      </td>
    </tr>
    <tr>
      <td  style="padding: 10px 10px 60px; font-size: 11px;">
        <b style="color: #1f72a3">NOTE :</b> Vestibulum vitae nisl sed felis tincidunt vulputate. Donec convallis, lorem vitae porttitor consectetur, quam metus fermentum augue
et congue magn enim quis sapien. In sed gravida lorem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Sed eu suscipit la
cus.  Vestibulum vitae nisl sed felis tincidunt vulputate. Donec convallis.
      </td>
    </tr>
    <tr>
      <td style="background-color: #173d71; color: #ffffff; font-size: 11px; text-align: center; padding: 15px;">
        Copyright 2016 . Thai Union Frozen Products PCL. by Thai Union Group. All rights reserved.
      </td>
    </tr>
  </tbody>
</table>

</body>

</html>
