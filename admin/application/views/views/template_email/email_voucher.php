<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title><?php echo $title; ?></title>
  <style>
   <?php
      if($fullname!="xxxxxxxx") {
    ?>
   body {
    padding: 0;
    margin: 0;
   }
    <?php
      }
    ?>
  </style>
</head>
<body>
  <table border="0" style="width: 660px; margin: 0 auto; font-family:Tahoma; font-size:11px;">
    <tr>
      <td><h1 style="margin:0;"><img src="<?php echo base_url().$image_logo; ?>" height="90" width="660" alt="DD4U"></h1></td>
    </tr>
    <tr>
      <td style="font-size:14px; padding-top:55px; text-align: center;">
        <div style="margin-bottom: 5px;"><b><?php echo $hello_user; ?></b> </div>
        <div style="margin-bottom: 30px;"><?php echo $thanks_for_confirm; ?></div>
      </td>
    </tr>
    <!--[if gte mso 9]>
      <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
        <v:fill type="tile" src="<?php echo site_url('public/images/email/voucherBG.png'); ?>" color="#FFFFFF"/>
      </v:background>
    <![endif]-->
    <tr>
      <td style="text-align: center; background-image:url(<?php echo site_url('public/images/email/voucherBG.png'); ?>); background-position: center; height: 320px; vertical-align: middle;" background="<?php echo site_url('public/images/email/voucherBG.png'); ?>">
        <div style="margin-bottom: 20px; font-size:16px;">GET  YOUR PROMO CODE</div>
        <div style="margin-bottom: 20px; color: #f59000; font-size: 30px;"><b><?php echo $promo_code; ?></b></div>
        <div style="margin-bottom: 10px; font-size:16px;">FOR GIFT VOUCHER </div>
        <div style="margin-bottom: 0; font-size:72px;"><b><?php echo $promo_price; ?></b></div>
        <div style="margin-bottom: 0; font-size:24px;">THB</div>
      </td>
    </tr>
    <tr>
      <td style="font-size: 14px; text-align: center;">
        <div style="color:#565656; margin:20px 0 40px;">EXPIRE IN <?php echo $expire_date; ?></div>
        <div style="margin-bottom: 10px;"><?php echo $promo_code_instruction; ?></div>
        <div style="margin-bottom: 10px;"><b><?php echo $thank_you; ?></b></div>
        <div style="margin-bottom: 10px;"><a href="<?php echo site_url(); ?>" style="color: #000000;" target="_blank"><b><?php echo $back_to; ?> <u>DD4U.COM</u></b></a></div>
      </td>
    </tr>
    <tr>
      <td>
        <div style="margin-bottom: 30px; font-size: 14px;"><b>DD4U.COM TEAM</b></div>
      </td>
    </tr>
    <tr>
      <td style="border-top:1px solid #cccccc; padding-top:10px;">
        <table border="0" style="width: 100%;">
          <tr>
            <td><?php echo $address; ?></td>
            <td style="text-align: right;">© 2016 DD4U ALL RIGHTS RESERVED</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>