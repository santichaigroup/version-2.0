<div class="span12"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-user"></i> System Users Group</h4> 
      <span class="tools">
      	
      </span>
      </div>
    <div class="widget-body">
     <form method="post" name="usergroup_listform" id="usergroup_listform" enctype="multipart/form-data">
      <?php if(@$success_message!=NULL){ ?>
      <div class="alert alert-success"> 
        <button class="close" data-dismiss="alert">×</button>
        <strong>Success !</strong> <?php echo $success_message; ?>
      </div>
      <?php } ?>
      <p>
      	 
        <div class="btn-group checktools">
         
         <button type="button" class="btn checkable-dropdown" data-toggle="dropdown"><i class="icon-user"></i> Action (<span class="select_count">0</span>) <span class="icon-caret-down"></span></button>
         <button type="button" onclick="$.getLocation('<?php echo admin_url("syssetting/usergroupadd/"); ?>');" class="btn"><i class="icon-plus-sign"></i> Add new user group</button>
         <ul class="dropdown-menu">
            <li><a href="javascript:;" onclick="selectgroup_delete();">Delete</a></li>
            <li><a href="javascript:;" onclick="selectgroup_suspend();">Suspend</a></li>
            <li><a href="javascript:;" onclick="selectgroup_unsuspend();">Unsuspend</a></li>
         </ul>
      </div>
      </p>
      <table class="table table-striped table-bordered" id="group_sort">
        <thead>
          <tr>
            <th style="width:8px"><input type="checkbox" class="usergroup-checkable" data-set=".group_id" /></th>
            <th><i class="icon-lock"></i> Group Name</th>
            <th><i class="icon-lock"></i> Superadmin Group</th>
            <th><i class="icon-check"></i> Status</th>
            <th><i class="icon-wrench"></i> Actions</th>
          </tr>
        </thead>
        <tbody>
         <?php foreach($this->admin_library->getAllGroup()->result_array() as $row){ ?>
          	<tr class="odd gradeX">
            	<td><input type="checkbox" value="<?php echo $row['group_id']; ?>"  class="group_id" name="group_id[]" id="group_id" /></td> 
                <td><?php echo $row['group_name']; ?></td>
                <td class="hidden-phone"><?php echo $row['group_superadmin']; ?></td>
              
                <td>
				<?php if($row['group_status']=="active"){ ?>
                	<span class="label label-success">Active</span>
                <?php }else{ ?>
                	<span class="label label-warning">Suspend</span>
                <?php } ?>
                </td>
                <td>
                	<div class="center">
                	
                    <a href="<?php echo admin_url("syssetting/usergroupedit/".$row['group_id']); ?>" class="icon huge"><i class="icon-pencil green"></i></a>&nbsp;
                    <a href="javascript:;" onclick="delete_group(<?php echo $row['group_id']; ?>);" class="icon huge"><i class="icon-remove red"></i></a>&nbsp;	
                    
                    </div>	
                </td>
            </tr>
         <?php } ?>
        </tbody>
      </table>
      <?php //echo create_pagination("syssetting/usergroup",200,20,4); ?>
       </form> 
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 
<script type="text/javascript">
var checktool='<div class="btn-group">'+
				'<button class="btn checkable-dropdown" data-toggle="dropdown">Action (<span class="select_count"></span>) <span class="caret"></span></button>'+
				'<ul class="dropdown-menu">'+
				'<li><a href="javascript:;" onclick="selectgroup_delete();">Delete <span class="select_count"></span> selected user(s)</a></li>'+
				'<li><a href="javascript:;" onclick="selectgroup_suspend();">Suspend <span class="select_count"></span> selected user(s)</a></li>'+
				'<li><a href="javascript:;" onclick="selectgroup_unsuspend();">Unsuspend <span class="select_count"></span> selected user(s)</a></li>'+
				'</ul>'+
				'</div>';
$(document).ready(function(e) {
    //$("#userTable").useDataTable();
	$("#usergroup_listform").handleformSubmit();
	$(".usergroup-checkable").handleCheckAll('.checkable-dropdown');
	$("#group_sort").dataTable(tablesetting);
});
function delete_group(user_id)
{
	if(confirm("Delete User Group !. Are you sure ?")){
	$("#usergroup_listform").attr("action",admin_url+"syssetting/usergroupdelete_user/"+user_id);
	$("#usergroup_listform").submit();
	}
}
function selectgroup_delete()
{
	if(confirm("Delete User Group !. Are you sure ?")){
	$("#usergroup_listform").attr("action",admin_url+"syssetting/usergrouphandle_delete");
	$("#usergroup_listform").submit();
	}
}
function selectgroup_suspend()
{
	$("#usergroup_listform").attr("action",admin_url+"syssetting/usergrouphandle_suspend");
	$("#usergroup_listform").submit();
}
function selectgroup_unsuspend()
{
	$("#usergroup_listform").attr("action",admin_url+"syssetting/usergrouphandle_unsuspend");
	$("#usergroup_listform").submit();
}
</script> 