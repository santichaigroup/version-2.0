<style type="text/css">
    
</style>

<div class="row">
  <?php echo form_open_multipart('', 'name="optionform" id="optionform"'); ?>
    <input type="hidden" name="lang_id" id="lang_id" value="<?php echo set_value("lang_id", $row['lang_id']); ?>">
    <input type="hidden" name="images" id="images" value="" />

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border toggle-click">
            <i class="glyphicon glyphicon-edit"></i>
            <h3 class="box-title">Infomation Box</h3>
        </div>
        <div class="box-body">

            <!--  Error Alert  -->
            <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
              <div class="alert alert-error">
                  <button class="close" data-dismiss="alert">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
              </div>
            <?php }?>
            <!--  Error Alert  -->

          <?php
          /************************* Infomation Box *************************/
          ?>
            <div class="col-md-12">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="content_subject" class="control-label">Title: &nbsp;<span style="color:#F00;">*</span></label>
                  <input type="text" name="content_subject" class="form-control" id="content_subject" placeholder="" value="<?php echo set_value("content_subject", $row['content_subject']); ?>">
                </div>

                <div class="form-group">
                  <label for="content_detail" class="control-label">Description: </label>
                    <textarea class="ckeditor" name="content_detail" id="content_detail" rows="10" cols="80"><?php echo set_value("content_detail", $row['content_detail']); ?></textarea>
                </div>
              </div>

              <div class="col-md-3">
                <div class="form-group">
                    <label for="menu_status" class="control-label">Language: </label><br>
                    <div class="controls btn-group">
                        <button class="btn dropdown-toggle" data-toggle="dropdown">
                            <img src="images/flags/<?php echo $this->admin_library->getLanguageflag($lang_id); ?> ">
                              <?php echo $this->admin_library->getLanguagename($lang_id); ?> 
                             <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                          <?php foreach($this->admin_library->getLanguageList() as $lang) {
                          if($row['lang_id'] <> $lang['lang_id']) {
                          ?>
                            <li>
                              <a href="<?php echo admin_url($this->menu['menu_link']."/edit/".$row['main_id']."/".$lang['lang_id']); ?>">
                                <img src="images/flags/<?php echo $lang['lang_flag']; ?>">&nbsp;<?php echo $lang['lang_name']; ?>
                              </a>
                            </li>
                          <?php }} ?>
                        </ul>
                    </div>
                </div>

                <!-- <div class="form-group">
                    <label for="create_by" class="control-label">เขียนโดย: </label>
                    <input type="text" class="form-control" name="post_by" id="post_by" disabled>
                </div> -->

                <div class="form-group">
                    <label for="post_date" class="control-label">Create date: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" name="post_date" id="post_date" value="<?php echo set_value("post_date",$row['post_date']); ?>"  readonly="readonly">
                    </div>
                </div>
                <?php
                if($row['update_by']) {
                ?>
                <div class="form-group">
                    <label for="upadte_by" class="control-label">Update by: </label>
                    <input type="text" class="form-control" name="upadte_by" id="upadte_by" value="<?php $update_name = $this->admin_library->getuserinfo($row['update_by']); echo $update_name['user_fullname']; ?>" disabled>
                </div>
                <?php
                }
                ?>
                <?php
                if($row['update_date']) {
                ?>
                <div class="form-group">
                    <label for="update_date" class="control-label">Update date: </label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="glyphicon glyphicon-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" name="update_date" id="update_date" value="<?php echo set_value("update_date",$row['update_date']); ?>" readonly="readonly">
                    </div>
                </div>
                <?php
                }
                ?>
                <div class="form-group">
                    <label for="content_status" class="control-label">Status: </label>

                    <select name="content_status" id="content_status" class="form-control">
                      <option value="active" <?php if(set_value("content_status", $row['content_status'])=="active"){ ?>selected="selected" <?php } ?>>แสดงข้อมูล</option>
                      <option value="pending" <?php if(set_value("content_status", $row['content_status'])=="pending"){ ?>selected="selected" <?php } ?>>ไม่แสดงข้อมูล</option>
                    </select>
                </div>

                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <a href="<?php echo admin_url($_menu_link."/index/".$lang_id); ?>" class="btn btn-block btn-danger">
                        <i class="fa fa-arrow-circle-left"></i>&nbsp;&nbsp;&nbsp;Back
                      </a>
                    </div>
                    <div class="col-md-6">
                      <button type="submit" class="btn btn-block btn-primary pull-right">
                        <i class="fa fa-save"></i>&nbsp;&nbsp;&nbsp;Save
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <?php
          /************************* Images & Files upload Box *************************/
          ?>
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="form-group">
                  <label for="content_subject" class="control-label">Picture highlight: </label>
                </div>

                <div class="row">
                  <div class="col-md-3">
                    <?php
                    if($row['content_thumbnail']) {
                    ?>
                    <a class="fancybox-buttons thumbnail" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row['content_thumbnail']['attachment_name']); ?>">
                      <img src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row['content_thumbnail']['attachment_name']); ?>" data-holder-rendered="true">
                    </a>
                    <?php
                    } else {
                    ?>
                    <a href="javascript:;" class="thumbnail">
                      <img src="<?php echo base_url("public/images/thumbnail-default.jpg"); ?>" data-holder-rendered="true">
                    </a>
                    <?php
                    }
                    ?>
                  </div>
                </div>

                <div class="form-group">
                  <label for="photo" class="control-label">Upload Pictures:</label>
                  <div class="input-group" style="width: 40%;">
                      <label class="input-group-btn">
                          <span class="btn btn-primary">
                              Browse&hellip; <input type="file" style="display: none;" name="image_thumb[]" id="image_thumb" accept="image/*" multiple>
                          </span>
                      </label>
                      <input type="text" class="form-control btn btn-block image_count" readonly>
                  </div>
                  <p class="help-block">*ขนาดรูป 640x360 พิกเซล(Pixel) ไม่เกิน 5 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .jpg, .png, .gif มากสุด 6 รูป</p>

                  <div class="row image_thumb"></div>

                  <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title">Pictures in Database</h3>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      </div>
                    </div>
                    <div class="box-body">
                      <div class="row image_thumb_list" id="sortable">
                        <?php foreach($rs_img->result_array() AS $row_img) { ?>
                        <?php if(!empty($row_img['attachment_name'])) { ?>
                        <div class="col-md-3 col-sm-3 col-xs-3 image_select">
                            <div class="thumbnail image_box">
                              <a class="fancybox-buttons" data-fancybox-group="button" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row_img['attachment_name']); ?>">
                                <img class="img-responsive" data-src="holder.js/100%x180" data-holder-rendered="true" src="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/images/".$row_img['attachment_name']); ?>" alt="" />
                              </a>
                              <div class="image_tools_select flex-center hover-opacity" onclick="delete_img('<?php echo $row_img['attachment_id']; ?>','<?php echo $row_img['default_main_id']; ?>','<?php echo $row_img['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');">
                                <i class="fa fa-remove text-red"></i>&nbsp;&nbsp;&nbsp;
                                <i class="glyphicon glyphicon-move text-light-blue" style="font-size: 45px;"></i>
                              </div>

                              <input type="hidden" name="attachment_id[]" value="<?php echo $row_img['attachment_id']; ?>"> 
                            </div>

                            <div style="margin-top: 0px; margin-bottom: 20px;">

                              <?php
                              $check_attachment_name  = explode("_", $row_img['attachment_name']);
                              $attachment_name        = ( count($check_attachment_name) > 1 ? $row_img['attachment_name'] : null )
                              ?>

                              <div class="form-group">
                                <label for="attachment_name" class="control-label">Image Name: </label>
                                <input type="text" name="attachment_name[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_name[".$row_img['attachment_id']."]", $attachment_name); ?>">
                                <input type="hidden" name="attachment_name_old[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo set_value("attachment_name[".$row_img['attachment_id']."]", $attachment_name); ?>">
                                <?php
                                if(!$attachment_name) {
                                ?>
                                _<?php echo $row_img['attachment_name_main']; ?>
                                <?php
                                }
                                ?>
                                <input type="hidden" name="attachment_name_main[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo $row_img['attachment_name_main']; ?>">
                                <input type="hidden" name="attachment_type[<?php echo $row_img['attachment_id']; ?>]" class="form-control" placeholder="" value="<?php echo $row_img['attachment_type']; ?>">
                              </div>

                              <div class="form-group">
                                <label for="attachment_detail" class="control-label">Image Alt: </label>
                                <textarea class="form-control" name="attachment_detail[<?php echo $row_img['attachment_id']; ?>]" rows="2"><?php echo set_value("attachment_detail[".$row_img['attachment_id']."]", $row_img['attachment_detail']); ?></textarea>
                              </div>

                              <a href="javascript:;" onclick="set_default_img('<?php echo $row_img['default_main_id']; ?>','<?php echo $row['content_id']; ?>','<?php echo $row_img['attachment_id'];?>','<?php echo $row['lang_id']; ?>');" class="btn btn-success btn-block text-center"><i class="glyphicon glyphicon-pushpin"></i>&nbsp;&nbsp;&nbsp;Set Image highlight
                              </a>
                            </div>
                        </div>
                        <?php } } ?>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label for="content_subject" class="control-label">Upload Files Attachment: </label>
                    <div class="input-group" style="width: 40%;">
                        <label class="input-group-btn">
                            <span class="btn btn-primary">
                                Browse&hellip; <input type="file" style="display: none;" name="file_thumb[]" id="file_thumb" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf" multiple>
                            </span>
                        </label>
                        <input type="text" class="form-control btn btn-block" readonly>
                    </div>
                    <p class="help-block">*ไฟล์ขนาดไม่เกิน 20 เมกะไบต์(MB) รองรับเฉพาะไฟล์ .pdf, doc, docx, rar, zip จำนวน 1 ไฟล์</p>
                </div>

                <div style="clear:both"></div>
                  <div class="row">
                      <?php foreach($rs_file->result_array() AS $row_file) { ?>
                      <?php if(!empty($row_file['attachment_name'])) { ?>
                      <?php
                        switch ($row_file['attachment_type']) {
                          case 'pdf':
                            $file_type = "pdf.png";
                            break;
                          case 'doc':
                            $file_type = "word.png";
                            break;
                          case 'docx':
                            $file_type = "word.png";
                            break;
                          case 'xls':
                            $file_type = "excel.png";
                            break;
                          case 'rar':
                            $file_type = "rar.png";
                            break;
                          case 'zip':
                            $file_type = "zip.png";
                            break;
                          
                        }
                      ?>

                          <div class="col-md-2">
                              <a class="thumbnail" target="_blank" href="<?php echo base_url("public/uploads/".$this->menu['menu_link']."/files/".$row_file['attachment_name']); ?>">
                                <img class="img-responsive" src="<?php echo base_url("public/images/icons/".$file_type); ?>" alt="" />
                              </a>
                              <p style="text-align: center;">
                                  <a href="javascript:;" onclick="delete_img(<?php echo $row_file['attachment_id']; ?>,<?php echo $row_file['default_main_id']; ?>,'<?php echo $row_file['attachment_name'];?>','<?php echo $row['lang_id']; ?>','images');" class="btn btn-danger">
                                    <i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;&nbsp;Delete
                                  </a>
                              </p>
                          </div>

                      <?php } } ?>
                  </div>
                <div style="clear:both"></div>
              </div>
            </div>
            
        </div>
      </div>
    </div>

  <?php
  /************************************************** SEO Box **************************************************/
  ?>

    <div class="col-md-12">
      <div class="box box-success">
        <div class="box-header with-border">

            <i class="glyphicon glyphicon-check"></i>
            <h3 class="box-title">SEO Box</h3>

        </div>
        <div class="box-body">

            <div class="form-group">
              <label for="content_seo" class="control-label">Url Website: </label>
              <input type="text" name="content_seo" class="form-control" id="content_seo" placeholder="" value="<?php echo set_value("content_seo", $row['content_seo']); ?>">
            </div>

            <div class="form-group">
              <label for="content_title" class="control-label">Meta Title: </label>
              <input type="text" name="content_title" class="form-control" id="content_title" placeholder="" value="<?php echo set_value("content_title", $row['content_title']); ?>">
            </div>

            <div class="form-group">
              <label for="content_description" class="control-label">Meta Description: </label>
              <textarea name="content_description" class="form-control" id="content_description" rows="3" placeholder="รายละเอียดเว็บไซต์"><?php echo set_value('content_description', $row['content_description']); ?></textarea>
            </div>

            <div class="form-group">
              <label for="content_keyword" class="control-label">Meta Keywords: </label>
              <input type="text" name="content_keyword" class="form-control" id="content_keyword" placeholder="" value="<?php echo set_value("content_keyword", $row['content_keyword']); ?>">
              <p class="help-block">ใช้ , คั่นระหว่างคำ</p>
            </div>
              
        </div>
      </div>
    </div>

  <?php echo form_close(); ?>
</div>

<script type="text/javascript">
  var storedFiles = [],
      items       = [];

  $(document).ready(function() {

    $(document).on('change', ':file', function() {

      var input     = $(this),
          numFiles  = input.get(0).files ? input.get(0).files.length : 1,
          label     = input.val().replace(/\\/g, '/').replace(/.*\//, '');
      input.trigger('fileselect', [numFiles, label]);
    });

    $(':file').on('fileselect', function(event, numFiles, label) {

      var input     = $(this).parents('.input-group').find(':text'),
          thumbnail = $('.image_thumb').find('.image_select').length,
          log       = numFiles > 1 ? (thumbnail + numFiles) + ' files selected' : label;
      if( input.length ) {
          input.val(log);
      } else {
          if( log ) alert(log);
      }
    });

    $("#image_thumb").on("change", handleFileSelect);
    $(".image_thumb").on("click", ".image_tools", removeFile);
  });
    
  function handleFileSelect(e) {

    $(".image_thumb").html('');
    $('#images').val('');
    storedFiles   = [];
    items         = [];
    var files     = e.target.files;
    var filesArr  = Array.prototype.slice.call(files);
    filesArr.forEach(function(f) {

      if(!f.type.match("image.*")) {
        return;
      }
      
      storedFiles.push(f);
      
      var reader    = new FileReader();
      reader.onload = function (e) {

        $(".image_thumb").append('<div class="col-md-3 col-sm-3 col-xs-3 image_select">'
          +'<div class="thumbnail image_box"><a href="javascript:;"><img src="'+e.target.result+'"></a>'
          +'<div class="image_tools flex-center hover-opacity" data-file="'+f.name+'"><i class="fa fa-remove text-red"></i></div>'
          +'</div></div>');
      }
      reader.readAsDataURL(f);
    });
  }

  function removeFile(e) {

    var file  = $(this).data("file");
    var i     = storedFiles.map(function (x) { return x.name; }).indexOf(file);

    items.push(i);
    $('#images').val(items);

    var thumbnail = $('.image_thumb').find('.image_select').length,
          log     = thumbnail > 0 ? (thumbnail-1) + ' files selected' : '';
          $('.image_count').val(log);

    $(this).parent().parent().remove();
  }

  function save_form()
  {
    $("form#optionform").submit();
  }

  function delete_img(attachment_id,default_main_id,attachment_name,lang_id,type_name)
  {
    if(confirm("Delete Data !. Are you sure ?")){

    $("body").on("click", ".image_tools_select", removeFile);
    
    $("#optionform").attr("action",admin_url+"<?php echo $this->menu['menu_link']; ?>/delete_img/"+attachment_id+"/"+default_main_id+"/"+attachment_name+"/"+lang_id+"/"+type_name);
    $("#optionform").submit();
    }
  }

  function set_default_img(default_main_id,content_id,attachment_id,lang_id)
  {
    if(confirm("Set picture highlight. Are you sure ?")){
    $("#optionform").attr("action",admin_url+"<?php echo $this->menu['menu_link']; ?>/set_default_img/"+default_main_id+"/"+content_id+"/"+attachment_id+"/"+lang_id);
    $("#optionform").submit();
    }
  }
</script>