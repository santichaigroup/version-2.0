<div class="span9"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-edit"></i> เพิ่ม<?php echo $_menu_name; ?> </h4> 
      <span class="tools">
      
      </span>
      </div>
    <div class="widget-body form">
    <form method="post" name="optionform" id="optionform" enctype="multipart/form-data" >

    <?php echo @$validation_errors; ?>
    <?php if(@$error_message!=NULL){ ?>
    	<div class="alert alert-error">
        	<button class="close" data-dismiss="alert">×</button>
            <strong>Error !</strong> <?php echo $error_message; ?>
        </div>
    <?php }?>
    		
			<div class="control-group">
                <label class="control-label" for="input1">E-mail Admin : &nbsp;<span style="color:#F00;">*</span></label>
                <div class="controls">
                    <input type="text" class="span10" id="content_email" name="content_email" value="<?php echo set_value("content_email"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">E-mail CC : (email_1@xxx.ocm , email_2@xxx.com)</label>
                <div class="controls">
                    <input type="text" class="span10" id="content_cc" name="content_cc" value="<?php echo set_value("content_cc"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">E-mail Account Paysbuy : &nbsp;<span style="color:#F00;">*</span></label>
                <div class="controls">
                    <input type="text" class="span10" id="content_mail_paysbuy" name="content_mail_paysbuy" value="<?php echo set_value("content_mail_paysbuy"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">หมายเลยประจาตัวของร้านค้า Paysbuy (psbID) : &nbsp;<span style="color:#F00;">*</span></label>
                <div class="controls">
                    <input type="text" class="span10" id="content_psbID" name="content_psbID" value="<?php echo set_value("content_psbID"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Pin Paysbuy : &nbsp;<span style="color:#F00;">*</span></label>
                <div class="controls">
                    <input type="text" class="span10" id="content_pin" name="content_pin" value="<?php echo set_value("content_pin"); ?>">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="input1">Secure Code Paysbuy : &nbsp;<span style="color:#F00;">*</span></label>
                <div class="controls">
                    <input type="text" class="span10" id="content_secureCode" name="content_secureCode" value="<?php echo set_value("content_secureCode"); ?>">
                </div>
            </div>

       
           
    </form>
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 
<div class="span3"> 
  <!-- BEGIN RECENT ORDERS PORTLET-->
  <div class="widget">
    <div class="widget-title widget-user">
      <h4><i class="icon-check"></i> เครื่องมือเพิ่มเติม</h4> 
      <span class="tools">
            
      </span>
      </div>
    <div class="widget-body form">
       
       
        <div class="form-actions">
        	<button type="submit" class="btn btn-success" onclick="save_form();">บันทึกการแก้ไข</button>
            <button type="button" class="btn btn-danger"  onclick="close_form();">ยกเลิก</button>		
        </div>
        
        
    </div>
  </div>
  <!-- END RECENT ORDERS PORTLET--> 
</div> 
<script type="text/javascript">
function save_form()
{
	$("#main_status").val($("#main_status_select").val());
	$("#main_date").val($("#main_date_edit").val());
	$("#content_keyword").val($("#content_keyword_edit").val());
	$("form#optionform").submit();	
}
function close_form()
{
	var x = confirm("Are you sure ?");
	if(x){
		$.getLocation(admin_url + "setemail/index");	
	}
}
</script>