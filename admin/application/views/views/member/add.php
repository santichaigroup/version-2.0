<div class="row">
  <form name="optionform" id="optionform" method="post" enctype="multipart/form-data" role="form" class="form-horizontal">

  <div class="col-md-8">

    <div class="box box-primary">
      <div class="box-header with-border toggle-click">

          <i class="glyphicon glyphicon-edit"></i>
          <h3 class="box-title">Profile Box</h3>

      </div>
      <div class="box-body">

          <!--  Error Alert  -->
          <?php if(@$error_message!=NULL || @$validation_errors!=NULL){ ?>
            <div class="alert alert-error">
                <button class="close" data-dismiss="alert">×</button>
                <strong>Error !</strong> <?php echo $error_message; ?> <?php echo @$validation_errors; ?>
            </div>
          <?php }?>
          <!--  Error Alert  -->

          <?php

            $member_id        = "";
            $member_class     = "";
            $email            = "";
            $telephone        = "";
            $fullname         = "";
            $gender           = "";
            $birthdate        = "";
            $is_enews         = "";
            $post_by          = "";
            $post_date        = "";
            $status           = "";
            $sequence         = "";

          ?>
          
            <!-- <div class="form-group">
              <label for="content_subject" class="col-sm-3 control-label">หมายเลขสมาชิก:</label>
              <div class="col-sm-7">
                <span class="form-control"><?php echo ($member_id ? $member_id : '-' ); ?></span>
              </div>
            </div> -->

            <div class="form-group">
              <label for="email" class="col-sm-3 control-label">ระดับสมาชิก:</label>
              <div class="col-sm-7">
                <span class="form-control">
                  Professional
                  <input type="hidden" name="member_class" value="2">
                </span>
              </div>
            </div>

            <div class="form-group">
              <label for="email" class="col-sm-3 control-label">อีเมล:</label>
              <div class="col-sm-7">
                <input type="email" name="email" value="" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label for="password" class="col-sm-7 control-label">* รหัสผ่านของสมาชิกจะถูกสุ่ม และส่งไปทางอีเมล</label>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">เบอร์โทรศัพท์:</label>
              <div class="col-sm-7">
                <input type="text" name="telephone" value="" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">ชื่อ - นามสกุล:</label>
              <div class="col-sm-7">
                <div class="row">
                  <div class="col-sm-6">
                    <input type="text" name="firstname" value="" class="form-control">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" name="lastname" value="" class="form-control">
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">เพศ:</label>
              <div class="col-sm-7">
              <?php
              switch ($gender) {
                case 'M':
                  $gender = "ชาย";
                  break;
                case 'F':
                  $gender = "หญิง";
                  break;

                default:
                  $gender = "ไม่ระบุ";
                  break;
              }
              ?>
                <select name="gender" class="form-control">
                    <option value="M">ชาย</option>
                    <option value="F">หญิง</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="telephone" class="col-sm-3 control-label">วัน/เดือน/ปี เกิด:</label>
              <div class="col-sm-7">
                
                <div class="row">
                  <div class="col-xs-4">
                    <select name="day" id="" class="form-control">
                      <option value="">วัน</option>
                      <?php
                        for($day=1; $day <= 31; $day++) {

                          if($day < 10){
                            $day = "0".$day;
                          }
                      ?>
                        <option value="<?php echo $day; ?>"><?php echo $day; ?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                  <div class="col-xs-4">
                    <select name="month" id="" class="form-control">
                      <option value="">เดือน</option>
                      <?php

                      // if($lang=="TH") {
                        $month = array('มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฏาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
                      // } else {
                      //   $month = array('January','February','March','April','May','June','July','August','September','October','November','December');
                      // }

                      for($y=1;$y<=12;$y++) {
                        
                        if($y < 10){
                          $yid = "0".$y;
                        } else {
                          $yid = $y;
                        }

                        echo "<option value='".$yid."'>".$month[$y-1]."</option>";

                      }
                      ?>
                    </select>
                  </div>
                  <div class="col-xs-4">
                    <select name="year" id="" class="form-control">
                      <option value="">ปี พ.ศ.</option>
                      <?php

                        for($year="2016";$year>="1936";$year--) {

                          echo "<option value='".$year."'>".($year+543)."</option>";
                        }

                      ?>
                    </select>
                  </div>
                </div>

              </div>
            </div>

      </div>
    </div>

  </div>

<?php
/************************************************** Address Box **************************************************/
?>

  <div class="col-md-4">

    <div class="box box-success">
      <div class="box-header with-border">

          <i class="glyphicon glyphicon-check"></i>
          <h3 class="box-title">Tools Box</h3>

      </div>
      <div class="box-body">

              <!-- <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">บ้านเลขที่/ หมู่บ้าน/ ชื่ออาคาร/ ชั้น:</label>
                <div class="col-sm-7">
                  <input type="text" name="address_no" class="form-control" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">ซอย:</label>
                <div class="col-sm-7">
                  <input type="text" name="address_soi" class="form-control" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">ถนน:</label>
                <div class="col-sm-7">
                  <input type="text" name="address_road" class="form-control" value="">
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">จังหวัด:</label>
                <div class="col-sm-7">
                  
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">อำเภอ / เขต:</label>
                <div class="col-sm-7">
                  
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">ตำบล / เเขวง:</label>
                <div class="col-sm-7">
                  
                </div>
              </div>

              <div class="form-group">
                <label for="telephone" class="col-sm-3 control-label">รหัสไปรษณีย์:</label>
                <div class="col-sm-7">
                  
                </div>
              </div> -->

          <div class="form-group">
              <label for="content_status_select" class="col-sm-4 control-label">สถานะสมาชิก: </label>
              <div class="col-sm-6">
                <select name="content_status_select" id="content_status_select" class="form-control">
                  <option value="active" <?php if(set_value("content_status")=="active"){ ?>selected="selected" <?php } ?>>อนุมัติสมาชิก</option>
                  <option value="pending" <?php if(set_value("content_status")=="pending"){ ?>selected="selected" <?php } ?>>รอการอนุมัติ</option>
                </select>
              </div>
          </div>


      </div>
      <div class="box-footer">
          <button type="submit" class="btn btn-success pull-right" onclick="save_form();">บันทึกข้อมูล</button>
          <a href="<?php echo admin_url(); ?>/<?php echo $_menu_link; ?>/index/<?php echo $lang_id; ?>" class="btn btn-danger">ย้อนกลับ</a>
      </div>
    </div>

  </div>

  </form>
</div>

<script type="text/javascript">
  function save_form()
  {
    $("form#optionform").submit();
  }

  function selectdata_suspend()
  {
    $("#usergroup_listform").attr("action",admin_url+"member/handle_suspend_member");
    $("#usergroup_listform").submit();
  }
  function selectdata_unsuspend()
  {
    $("#usergroup_listform").attr("action",admin_url+"member/handle_unsuspend_member");
    $("#usergroup_listform").submit();
  }
</script>