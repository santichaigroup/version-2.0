<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo $asset_url; ?>" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
    <!-- DatePicker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- daterange picker -->
    <link rel="stylesheet" href="plugins/bootstrap-daterangepicker/daterangepicker.css">
    <!-- JsTree -->
    <link rel="stylesheet" href="plugins/jstree/dist/themes/default/style.min.css">
    <!-- Multiselect -->
    <link rel="stylesheet" href="plugins/multiselect/css/ui.multiselect.css">
    <!-- <link rel="stylesheet" href="plugins/jQueryUI/themes/flick/jquery-ui.min.css" /> -->
    <!-- Tag It -->
    <link rel="stylesheet" href="plugins/tag-it/css/jquery.tagit.css" />
    <!-- Color Picker -->
    <link rel="stylesheet" href="plugins/colorpicker/bootstrap-colorpicker.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css">
    <!-- Chart -->
    <script src="plugins/chartjs/Chart.min.js"></script>

    <!-- Theme style -->
    <link rel="stylesheet" href="css/adminLTE.min.css">
    <!-- adminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="css/skins/_all-skins.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="plugins/jQueryUI/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript">
      var admin_url = "<?php echo $admin_url; ?>/";
      var base_url = "<?php echo $base_url; ?>";
      var site_url = "<?php echo $site_url; ?>";
      var asset_url = "<?php echo $asset_url; ?>";
      $(function(){
        $.ajaxSetup({
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        });
      });
    </script> 
    <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.columnFilter.js" type="text/javascript"></script>
    <script src="plugins/ckeditor/ckeditor.js"></script>
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- date-range-picker -->
    <script src="plugins/moment/moment.min.js"></script>
    <script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- Fancy Box -->
    <script type="text/javascript" src="plugins/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="plugins/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>

    <link rel="stylesheet" type="text/css" href="plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
    <link rel="stylesheet" type="text/css" href="plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />

    <link rel="stylesheet" type="text/css" href="plugins/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
    <script type="text/javascript" src="plugins/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

    <script type="text/javascript" src="plugins/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">

    <script type="text/javascript" src="plugins/jstree/dist/jstree.min.js"></script>

    <script type="text/javascript" src="plugins/multiselect/plugins/localisation/jquery.localisation-min.js"></script>
    <script type="text/javascript" src="plugins/multiselect/plugins/ui.multiselect.js"></script>

    <script type="text/javascript" src="plugins/tag-it/js/tag-it.min.js"></script>
    
    <script type="text/javascript" src="plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <script type="text/javascript" src="plugins/input-mask/v3/inputmask.js"></script>
    <script type="text/javascript" src="plugins/input-mask/v3/jquery.inputmask.js"></script>
    <script type="text/javascript" src="plugins/input-mask/v3/inputmask.numeric.extensions.js"></script>

    <script type="text/javascript" src="js/chosen.jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/chosen.css" />
    <link rel="stylesheet" type="text/css" href="css/styleAdmin.css" />

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

<?php
    ##############################################   Header Menu   #############################################
?>

      <header class="main-header">

        <!-- Logo -->
        <a href="<?php echo admin_url(); ?>" class="logo">
          <span class="logo-mini"><b>A</b>min</span>

          <span class="logo-lg"><b><?php echo $this->admin_library->getCompanyName(); ?></b></span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <?php echo $header_bar; ?>

      </header>

<?php
    ##############################################   Left Menu   #############################################
?>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <section class="sidebar">
          
          <div class="user-panel">
            <span class="user-company">
              
                <?php $group_name = $this->admin_library->getGroupDetail($user_info['user_group']); echo $group_name['group_name']; ?>
              
            </span>
          </div>

          <!-- sidebar menu: -->

          <?php echo $left_menu; ?>

          <!-- sidebar menu: -->
          
        </section>
      </aside>

<?php
    ##############################################   Main Menu   #############################################
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">

          <?php echo $page_navi; ?>

          <!-- Main content -->
          <section class="content">

              <!-- Main Body -->
              
              <?php echo $body_entry; ?>

              <!-- Main Body -->

          </section>
          <!-- Main Body -->

      </div>
      <!-- Content Wrapper. Contains page content -->

<?php
    ##############################################   Footer Menu   #############################################
?>

<!--       <footer class="main-footer">
        <div class="pull-right hidden-xs">

        </div>
      </footer> -->

<?php
    ##############################################   Tools Menu   #############################################
?>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs"></ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <!-- Themes -->
          </div>
          <!-- Home tab content -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

<?php
    ##############################################   JS   #############################################
?>

    <!-- adminLTE App -->
    <script src="js/app.min.js"></script>
    <!-- adminLTE for demo purposes -->
    <script src="js/themes.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <!-- CKEDITOR -->
    <script type="text/javascript">
      $(function () {
        
        //Initialize Select2 Elements
        $(".select2").select2();
        // Date Picker
        $('.date-picker').datepicker({
          format : "dd-mm-yyyy"
        });
        // Date Rang Picker
        $('.dateRangePicker').daterangepicker({
          autoUpdateInput: false,
          "drops": "down",
          "autoApply": false,
          timePicker: false,
          "timePicker24Hour": false,
          locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
          }
        });
        // Fancy box
        $('.fancybox-buttons').fancybox({
          openEffect  : 'none',
          closeEffect : 'none',

          prevEffect : 'none',
          nextEffect : 'none',

          closeBtn  : true,

          helpers : {
            title : {
              type : 'inside'
            },
            buttons : {}
          },

          afterLoad : function() {
            this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
          }
        });
        // ui move
        $( "#sortable" ).sortable({
            appendTo: "body",
            helper: "clone"
        }).disableSelection();
        // $( "#sortable" ).sortable();
        // $( "#sortable" ).disableSelection();
      });
    </script>

  </body>
</html>