<?php
class Core_library {

	private $ci;
	private $setting = array();
	public function __construct()
	{
		$this->ci = & get_instance();
		$this->setting['body_entry']	= "";
		$this->setting['asset_url'] 	= base_url("public/core/");
		$this->setting['site_url'] 		= site_url();
		$this->setting['base_url'] 		= base_url();
		$this->setting['current_url'] 	= site_url($this->ci->uri->ruri_string());
	}

	public function view($file,$data=array(),$output=false)
	{
		if($data) {
			$this->setting = array_merge($this->setting,$data);
		}

		$this->setting['header'] 		= $this->ci->parser->parse("include/header",$this->setting,true);
		$this->setting['inc_header'] 	= $this->ci->parser->parse("include/inc_header",$this->setting,true);
		$this->setting['footer'] 		= $this->ci->parser->parse("include/footer",$this->setting,true);
		$this->setting['inc_globalJS'] 	= $this->ci->parser->parse("include/inc_globalJS",$this->setting,true);

		foreach($_GET as $key=>$val) {
			$this->setting['get'][$key]=$val;
		}
		$res = $this->ci->load->view($file,$this->setting,true);
		if($output==false) {
			$this->setting['body_entry'] = $res;
		}

		return $res;
	}

	public function output($uri=null, $expires=0)
	{
		if($uri) {

			$check_cache = $this->ci->cache_library->get($uri);

			if ($check_cache === false) {

				$this->ci->load->view("include/template",$this->setting);
				$html['html'] = $this->ci->load->view("include/template",$this->setting,true);

				$this->ci->cache_library->write($html, $uri, $expires); // 5 minute = 300
			} else {

				echo $this->ci->cache_library->get($uri);
			}
		} else {

			$this->ci->load->view("include/template",$this->setting);
		}
	}
}