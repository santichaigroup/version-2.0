<?php
class Dashboard extends CI_Controller{
	var $color;
	public function __construct()
	{
		parent::__construct();
		$this->load->library('admin_library');
		$this->admin_library->forceLogin();

		$this->path 	= 	$this->uri->ruri_string();
	}

	function index()
	{
		$this->admin_library->setTitle("Dashboard",'icon-home'); 
		$this->admin_library->setDetail("Stat &amp; Summary");
		// $this->admin_library->view("dashboard/index", $this->_data); 
		$this->admin_library->output($this->path);
	}
}  